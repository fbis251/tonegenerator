package com.fernandobarillas.tonegenerator;

import com.fernandobarillas.tonegenerator.model.Tone;
import com.fernandobarillas.tonegenerator.sound.ToneGenerator;

import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.LineUnavailableException;

public class Main {

    public static void main(String[] args) {

        List<Tone> octaves = new ArrayList<>();

        int loops = 4;
        for (int i = 0; i < loops; i++) {
            octaves.add(new Tone(Tone.C1, 125));
            octaves.add(new Tone(Tone.C2, 125));
            octaves.add(new Tone(Tone.C3, 125));
            octaves.add(new Tone(Tone.C4, 125));
            octaves.add(new Tone(Tone.C5, 125));
            octaves.add(new Tone(Tone.C6, 125));
            octaves.add(new Tone(Tone.C7, 125));
            octaves.add(new Tone(Tone.C8, 125));
        }
        octaves.add(new Tone(Tone.C4, 250));

        List<Tone> tones = new ArrayList<>();
        tones.add(new Tone(Tone.C4, 250));
        tones.add(new Tone(Tone.REST, 500));
        tones.add(new Tone(Tone.D4, 250));
        tones.add(new Tone(Tone.REST, 500));
        tones.add(new Tone(Tone.E4, 250));
        tones.add(new Tone(Tone.REST, 500));
        tones.add(new Tone(Tone.F4, 250));
        tones.add(new Tone(Tone.REST, 500));
        tones.add(new Tone(Tone.G4, 250));
        tones.add(new Tone(Tone.A4, 250));
        tones.add(new Tone(Tone.B4, 250));
        tones.add(new Tone(Tone.C5, 250));
        tones.add(new Tone(Tone.B4, 250));
        tones.add(new Tone(Tone.A4, 250));
        tones.add(new Tone(Tone.G4, 250));
        tones.add(new Tone(Tone.F4, 250));
        tones.add(new Tone(Tone.E4, 250));
        tones.add(new Tone(Tone.D4, 250));
        tones.add(new Tone(Tone.C4, 250));

        tones.add(new Tone(Tone.C4, 500));
        tones.add(new Tone(Tone.E4, 500));
        tones.add(new Tone(Tone.G4, 500));
        tones.add(new Tone(Tone.C5, 500));
        tones.add(new Tone(Tone.G4, 500));
        tones.add(new Tone(Tone.E4, 500));
        tones.add(new Tone(Tone.C4, 1000));
        tones.add(new Tone(Tone.G3, 1000));
        tones.add(new Tone(Tone.C4, 2000));

        try {
            ToneGenerator toneGenerator = new ToneGenerator();
            toneGenerator.playTones(octaves);

            // 1 second of silence
            toneGenerator.playTone(Tone.REST, 1000);
            toneGenerator.playTones(tones);

            // 1 second of silence
            toneGenerator.playTone(Tone.REST, 1000);
            toneGenerator.sweep();

            // Deallocate resources as they are no longer needed
            toneGenerator.destroy();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }
}
