package com.fernandobarillas.tonegenerator.sound;

import com.fernandobarillas.tonegenerator.model.Tone;

import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

/**
 * Utility class for generating tones
 */
public class ToneGenerator {

    // How many samples per second to be used when generating the tones
    private static final int SAMPLE_RATE = 44100;

    // Amplitude of sine wave, between -128 and 128 since we are dealing with a byte type
    private static final double AMPLITUDE = 100;

    private SourceDataLine sdl;

    public ToneGenerator() throws LineUnavailableException {
        AudioFormat af = new AudioFormat((float) SAMPLE_RATE, 8, 1, true, false);
        sdl = AudioSystem.getSourceDataLine(af);
        sdl.open();
        sdl.start();
    }

    /**
     * Deallocates resources dedicated to playing sound.
     */
    public void destroy() {
        sdl.stop();
    }

    /**
     * Creates a sound buffer for a frequency and duration
     *
     * @param frequency            The frequency of the note to play
     * @param durationMilliseconds The amount of time the tone should play for in milliseconds
     * @return A buffer containing the generated sound
     */
    public byte[] makeSoundBuffer(double frequency, double durationMilliseconds) {
        double bufferLength = durationMilliseconds * (float) SAMPLE_RATE / 1000;
        byte[] buffer = new byte[(int) Math.ceil(bufferLength)];

        // Get the sin value as x increases
        for (int x = 0; x < bufferLength; x++) {
            double angle = x / (SAMPLE_RATE / frequency) * 2.0 * Math.PI;
            buffer[x] = (byte) (Math.sin(angle) * AMPLITUDE);
        }
        return buffer;
    }

    /**
     * Plays one tone of a frequency for duration milliseconds
     *
     * @param frequency The frequency of the tone to generate
     * @param duration  How many milliseconds to play the tone for
     */
    public void playTone(double frequency, int duration) {
        byte[] buffer = makeSoundBuffer(frequency, duration);
        sdl.write(buffer, 0, buffer.length);
        sdl.drain();
    }

    /**
     * Plays all the passed-in tones in sequence
     *
     * @param tones The tones to play in sequence
     */
    public void playTones(List<Tone> tones) {
        List<Byte> bytes = new ArrayList<>();
        for (Tone tone : tones) {
            byte[] buffer = makeSoundBuffer(tone.getFrequency(), tone.getDuration());
            for (byte b : buffer) {
                bytes.add(b);
            }
        }

        byte[] buffer = new byte[bytes.size()];
        int byteCount = 0;
        for (Byte currentByte : bytes) {
            buffer[byteCount] = currentByte;
            byteCount++;
        }
        sdl.write(buffer, 0, buffer.length);
        sdl.drain();
    }

    /**
     * Plays every note from the lowest (C0) to the highest (B8) on a piano.
     */
    public void sweep() {
        List<Tone> tones = new ArrayList<>();
        tones.add(new Tone(Tone.C0, 62.5));
        tones.add(new Tone(Tone.Cs0, 62.5));
        tones.add(new Tone(Tone.D0, 62.5));
        tones.add(new Tone(Tone.Ds0, 62.5));
        tones.add(new Tone(Tone.E0, 62.5));
        tones.add(new Tone(Tone.F0, 62.5));
        tones.add(new Tone(Tone.Fs0, 62.5));
        tones.add(new Tone(Tone.G0, 62.5));
        tones.add(new Tone(Tone.Gs0, 62.5));
        tones.add(new Tone(Tone.A0, 62.5));
        tones.add(new Tone(Tone.As0, 62.5));
        tones.add(new Tone(Tone.B0, 62.5));
        tones.add(new Tone(Tone.C1, 62.5));
        tones.add(new Tone(Tone.Cs1, 62.5));
        tones.add(new Tone(Tone.D1, 62.5));
        tones.add(new Tone(Tone.Ds1, 62.5));
        tones.add(new Tone(Tone.E1, 62.5));
        tones.add(new Tone(Tone.F1, 62.5));
        tones.add(new Tone(Tone.Fs1, 62.5));
        tones.add(new Tone(Tone.G1, 62.5));
        tones.add(new Tone(Tone.Gs1, 62.5));
        tones.add(new Tone(Tone.A1, 62.5));
        tones.add(new Tone(Tone.As1, 62.5));
        tones.add(new Tone(Tone.B1, 62.5));
        tones.add(new Tone(Tone.C2, 62.5));
        tones.add(new Tone(Tone.Cs2, 62.5));
        tones.add(new Tone(Tone.D2, 62.5));
        tones.add(new Tone(Tone.Ds2, 62.5));
        tones.add(new Tone(Tone.E2, 62.5));
        tones.add(new Tone(Tone.F2, 62.5));
        tones.add(new Tone(Tone.Fs2, 62.5));
        tones.add(new Tone(Tone.G2, 62.5));
        tones.add(new Tone(Tone.Gs2, 62.5));
        tones.add(new Tone(Tone.A2, 62.5));
        tones.add(new Tone(Tone.As2, 62.5));
        tones.add(new Tone(Tone.B2, 62.5));
        tones.add(new Tone(Tone.C3, 62.5));
        tones.add(new Tone(Tone.Cs3, 62.5));
        tones.add(new Tone(Tone.D3, 62.5));
        tones.add(new Tone(Tone.Ds3, 62.5));
        tones.add(new Tone(Tone.E3, 62.5));
        tones.add(new Tone(Tone.F3, 62.5));
        tones.add(new Tone(Tone.Fs3, 62.5));
        tones.add(new Tone(Tone.G3, 62.5));
        tones.add(new Tone(Tone.Gs3, 62.5));
        tones.add(new Tone(Tone.A3, 62.5));
        tones.add(new Tone(Tone.As3, 62.5));
        tones.add(new Tone(Tone.B3, 62.5));
        tones.add(new Tone(Tone.C4, 62.5));
        tones.add(new Tone(Tone.Cs4, 62.5));
        tones.add(new Tone(Tone.D4, 62.5));
        tones.add(new Tone(Tone.Ds4, 62.5));
        tones.add(new Tone(Tone.E4, 62.5));
        tones.add(new Tone(Tone.F4, 62.5));
        tones.add(new Tone(Tone.Fs4, 62.5));
        tones.add(new Tone(Tone.G4, 62.5));
        tones.add(new Tone(Tone.Gs4, 62.5));
        tones.add(new Tone(Tone.A4, 62.5));
        tones.add(new Tone(Tone.As4, 62.5));
        tones.add(new Tone(Tone.B4, 62.5));
        tones.add(new Tone(Tone.C5, 62.5));
        tones.add(new Tone(Tone.Cs5, 62.5));
        tones.add(new Tone(Tone.D5, 62.5));
        tones.add(new Tone(Tone.Ds5, 62.5));
        tones.add(new Tone(Tone.E5, 62.5));
        tones.add(new Tone(Tone.F5, 62.5));
        tones.add(new Tone(Tone.Fs5, 62.5));
        tones.add(new Tone(Tone.G5, 62.5));
        tones.add(new Tone(Tone.Gs5, 62.5));
        tones.add(new Tone(Tone.A5, 62.5));
        tones.add(new Tone(Tone.As5, 62.5));
        tones.add(new Tone(Tone.B5, 62.5));
        tones.add(new Tone(Tone.C6, 62.5));
        tones.add(new Tone(Tone.Cs6, 62.5));
        tones.add(new Tone(Tone.D6, 62.5));
        tones.add(new Tone(Tone.Ds6, 62.5));
        tones.add(new Tone(Tone.E6, 62.5));
        tones.add(new Tone(Tone.F6, 62.5));
        tones.add(new Tone(Tone.Fs6, 62.5));
        tones.add(new Tone(Tone.G6, 62.5));
        tones.add(new Tone(Tone.Gs6, 62.5));
        tones.add(new Tone(Tone.A6, 62.5));
        tones.add(new Tone(Tone.As6, 62.5));
        tones.add(new Tone(Tone.B6, 62.5));
        tones.add(new Tone(Tone.C7, 62.5));
        tones.add(new Tone(Tone.Cs7, 62.5));
        tones.add(new Tone(Tone.D7, 62.5));
        tones.add(new Tone(Tone.Ds7, 62.5));
        tones.add(new Tone(Tone.E7, 62.5));
        tones.add(new Tone(Tone.F7, 62.5));
        tones.add(new Tone(Tone.Fs7, 62.5));
        tones.add(new Tone(Tone.G7, 62.5));
        tones.add(new Tone(Tone.Gs7, 62.5));
        tones.add(new Tone(Tone.A7, 62.5));
        tones.add(new Tone(Tone.As7, 62.5));
        tones.add(new Tone(Tone.B7, 62.5));
        tones.add(new Tone(Tone.C8, 62.5));
        tones.add(new Tone(Tone.Cs8, 62.5));
        tones.add(new Tone(Tone.D8, 62.5));
        tones.add(new Tone(Tone.Ds8, 62.5));
        tones.add(new Tone(Tone.E8, 62.5));
        tones.add(new Tone(Tone.F8, 62.5));
        tones.add(new Tone(Tone.Fs8, 62.5));
        tones.add(new Tone(Tone.G8, 62.5));
        tones.add(new Tone(Tone.Gs8, 62.5));
        tones.add(new Tone(Tone.A8, 62.5));
        tones.add(new Tone(Tone.As8, 62.5));
        tones.add(new Tone(Tone.B8, 62.5));
        playTones(tones);
    }

    // https://stackoverflow.com/questions/1932490/java-generating-sound/1932537#1932537
    private void example() throws LineUnavailableException {
        byte[] buf = new byte[1];
        AudioFormat af = new AudioFormat((float) 44100, 8, 1, true, false);
        SourceDataLine sdl = AudioSystem.getSourceDataLine(af);
        sdl.open();
        sdl.start();
        for (int i = 0; i < 1000 * (float) 44100 / 1000; i++) {
            double angle = i / ((float) 44100 / 440) * 2.0 * Math.PI;
            buf[0] = (byte) (Math.sin(angle) * 100);
            sdl.write(buf, 0, 1);
        }
        sdl.drain();
        sdl.stop();
    }
}
