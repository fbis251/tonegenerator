package com.fernandobarillas.tonegenerator.model;

public class Tone {
    public static final double B8  = 7902.133;
    public static final double As8 = 7458.62;
    public static final double A8  = 7040;
    public static final double Gs8 = 6644.875;
    public static final double G8  = 6271.927;
    public static final double Fs8 = 5919.911;
    public static final double F8  = 5587.652;
    public static final double E8  = 5274.041;
    public static final double Ds8 = 4978.032;
    public static final double D8  = 4698.636;
    public static final double Cs8 = 4434.922;
    public static final double C8  = 4186.009;
    public static final double B7  = 3951.066;
    public static final double As7 = 3729.31;
    public static final double A7  = 3520;
    public static final double Gs7 = 3322.438;
    public static final double G7  = 3135.963;
    public static final double Fs7 = 2959.955;
    public static final double F7  = 2793.826;
    public static final double E7  = 2637.02;
    public static final double Ds7 = 2489.016;
    public static final double D7  = 2349.318;
    public static final double Cs7 = 2217.461;
    public static final double C7  = 2093.005;
    public static final double B6  = 1975.533;
    public static final double As6 = 1864.655;
    public static final double A6  = 1760;
    public static final double Gs6 = 1661.219;
    public static final double G6  = 1567.982;
    public static final double Fs6 = 1479.978;
    public static final double F6  = 1396.913;
    public static final double E6  = 1318.51;
    public static final double Ds6 = 1244.508;
    public static final double D6  = 1174.659;
    public static final double Cs6 = 1108.731;
    public static final double C6  = 1046.502;
    public static final double B5  = 987.7666;
    public static final double As5 = 932.3275;
    public static final double A5  = 880;
    public static final double Gs5 = 830.6094;
    public static final double G5  = 783.9909;
    public static final double Fs5 = 739.9888;
    public static final double F5  = 698.4565;
    public static final double E5  = 659.2551;
    public static final double Ds5 = 622.254;
    public static final double D5  = 587.3295;
    public static final double Cs5 = 554.3653;
    public static final double C5  = 523.2511;
    public static final double B4  = 493.8833;
    public static final double As4 = 466.1638;
    public static final double A4  = 440;
    public static final double Gs4 = 415.3047;
    public static final double G4  = 391.9954;
    public static final double Fs4 = 369.9944;
    public static final double F4  = 349.2282;
    public static final double E4  = 329.6276;
    public static final double Ds4 = 311.127;
    public static final double D4  = 293.6648;
    public static final double Cs4 = 277.1826;
    public static final double C4  = 261.6256;
    public static final double B3  = 246.9417;
    public static final double As3 = 233.0819;
    public static final double A3  = 220;
    public static final double Gs3 = 207.6523;
    public static final double G3  = 195.9977;
    public static final double Fs3 = 184.9972;
    public static final double F3  = 174.6141;
    public static final double E3  = 164.8138;
    public static final double Ds3 = 155.5635;
    public static final double D3  = 146.8324;
    public static final double Cs3 = 138.5913;
    public static final double C3  = 130.8128;
    public static final double B2  = 123.4708;
    public static final double As2 = 116.5409;
    public static final double A2  = 110;
    public static final double Gs2 = 103.8262;
    public static final double G2  = 97.99886;
    public static final double Fs2 = 92.49861;
    public static final double F2  = 87.30706;
    public static final double E2  = 82.40689;
    public static final double Ds2 = 77.78175;
    public static final double D2  = 73.41619;
    public static final double Cs2 = 69.29566;
    public static final double C2  = 65.40639;
    public static final double B1  = 61.73541;
    public static final double As1 = 58.27047;
    public static final double A1  = 55;
    public static final double Gs1 = 51.91309;
    public static final double G1  = 48.99943;
    public static final double Fs1 = 46.2493;
    public static final double F1  = 43.65353;
    public static final double E1  = 41.20344;
    public static final double Ds1 = 38.89087;
    public static final double D1  = 36.7081;
    public static final double Cs1 = 34.64783;
    public static final double C1  = 32.7032;
    public static final double B0  = 30.86771;
    public static final double As0 = 29.13524;
    public static final double A0  = 27.5;
    public static final double Gs0 = 25.95654;
    public static final double G0  = 24.49971;
    public static final double Fs0 = 23.12465;
    public static final double F0  = 21.82676;
    public static final double E0  = 20.60172;
    public static final double Ds0 = 19.44544;
    public static final double D0  = 18.35405;
    public static final double Cs0 = 17.32391;
    public static final double C0  = 16.3516;

    public static final double REST = 0;

    private double frequency;
    private double duration;

    public Tone(double frequency, double duration) {
        this.frequency = frequency;
        this.duration = duration;
    }

    public double getDuration() {
        return duration;
    }

    public double getFrequency() {
        return frequency;
    }
}
